from setuptools import setup, find_packages

setup(
   name='pepnn',
   version='1.0',
   description='A module for peptide binding site prediction',
   author='Osama Abdin',
   author_email='osama.abdin@kimlab.org',
   packages=  find_packages(),
   include_package_data=True,

   install_requires=['torch', 'biopython', 'numpy', 'pandas', 'scipy', 'transformers'],
   scripts=['pepnn_struct/scripts/predict_binding_site.py',
            'pepnn_seq/scripts/predict_binding_site.py',
           ]
)