from Bio import PDB
import numpy as np

class ValidAtoms(PDB.Select):
    
    def __init__(self, chains=[]):
         super().__init__()
         
         self.chains = chains
    def accept_model(self, model):
        return 1 if model.id == 0 else 0
    
    
    def accept_chain(self, chain):
       
        if len(self.chains) == 0:
            return 1
        else:
            return chain.id in self.chains
        
    def accept_atom(self, atom):
        
        return atom.get_bfactor() <= 1
    
    
def output_prediction(input_file, input_chain, model_outputs, output_file):
    
    predictions = np.exp(model_outputs[0])[:, 1]
    
    parser = PDB.PDBParser()
    structure = parser.get_structure(input_file, input_file)
    
    protein_chain = structure.get_list()[0].__getitem__(input_chain)
    
    protein_residues = protein_chain.get_list()
    
    
    curr_res = 0
    
    for atom in structure.get_atoms():
        
        atom.set_bfactor(0)
        
    for res in protein_residues:
        
        if not PDB.Polypeptide.is_aa(res.get_resname(), standard=True):
            for a_i, atom in enumerate(res.get_atoms()):
                  
                atom.set_bfactor(0)
            continue
        
      
        
       
        all_atoms = [atom.get_name().strip() for atom in res.get_atoms()]
        
        if "C" not in all_atoms or "CA" not in all_atoms or "N" not in all_atoms:
            for a_i, atom in enumerate(res.get_atoms()):
                  
                atom.set_bfactor(0)
            continue
        
        for a_i, atom in enumerate(res.get_atoms()):
            atom.set_bfactor(predictions[curr_res])
            
        curr_res += 1
            
    io = PDB.PDBIO()
        
    io.set_structure(structure)
    selector = ValidAtoms()
    io.save(output_file, select=selector)